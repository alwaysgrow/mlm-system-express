const knex = require("knex")({
  client: "mysql",
  connection: {
    host: "localhost",
    port: 3306,
    user: "root",
    password: "secret",
    database: "mlm_system"
  },
  debug: true,
  pool: {
    min: 1,
    max: 20
  }
});
module.exports = knex;