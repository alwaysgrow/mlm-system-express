var express = require("express");
var router = express.Router();
const knex = require("../connection/db-connect");

router.post("/add", async function (req, res, next) {
  const dataPacket = req.body;

  knex
    .from("customer")
    .select("id", "parent_id", "level")
    .where("id", req.body.parent_id)
    .then(async (rows) => {
      if (rows.length === 1) {
        const data = await knex
          .insert({ ...dataPacket, parent_id: rows[0].level == 0 ? rows[0].id : rows[0].parent_id })
          .into("customer")
          .returning("id");
        if (data.length === 1) {
          const parentId = JSON.parse(rows[0].parent_id);
          const updateCustomerDetails = await knex("customer")
            .where("id", data[0])
            .update({
              parent_id: JSON.stringify([...parentId, data[0]]),
              level: [...parentId, data[0]].length - 1
            })
            .returning("id");
          if (updateCustomerDetails) {
            return res.status(200).json({
              message: "successfully added customer"
            });
          }

          return res.status(400).json({
            message: "something went's wrong"
          });
        }
        return res.status(400).json({
          message: "something went's wrong"
        });
      }
      return res.status(400).json({
        message: "something went's wrong"
      });
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
});

router.patch("/update", async function (req, res, next) {
  const dataPacket = { ...req.body };
  try {
    const updateCustomerDetails = await knex("customer").where("id", req.body.id).update(dataPacket).returning("id");
    return res.status(200).json({
      message: "details updated successfully"
    });
  } catch (err) {
    console.log(err);
    return res.status(200).json({
      message: "something went's wrong"
    });
  }
});

router.get("/all", async function (req, res, next) {
  knex
  .from("customer")
  .select("*")
  .then(async (rows) => {
    if (rows.length > 0) {
      const customer =[]
      rows.forEach(element => {
        const filterData = rows.filter(item => JSON.parse(item.parent_id).includes(element.id))
        const noOfDecedent = filterData.length>0?filterData.length-1:filterData.length
        customer.push({
          ...element,
          noOfDecedent
        })
      });
      return res.status(400).json(customer);
    }
    return res.status(400).json({
      message: "not any customer"
    });
  })
  .catch((err) => {
    console.log(err);
    throw err;
  });
});

router.post("/purchase", async function (req, res, next) {
  knex
    .from("products")
    .select("*")
    .where("id", req.body.product_id)
    .then(async (rows) => {
      if (rows.length > 0) {
        const productDetail = rows[0];
        const earnPoints = (productDetail.price * 5) / 100;
        const data = await knex.insert(req.body).into("customer_purchase").returning("id");
        if (data.length) {
          await knex
            .from("customer")
            .select("loyalty_points")
            .where("id", req.body.customer_id)
            .then(async (row) => {
              if (row.length > 0) {
                const updateLoyaltyPoints = await knex("customer")
                  .where("id", req.body.customer_id)
                  .update({
                    loyalty_points: row[0].loyalty_points ? row[0].loyalty_points + earnPoints : earnPoints
                  })
                  .returning("id");
                if (updateLoyaltyPoints) {
                  return res.status(200).json({ message: "successfully purchase item" });
                }
              }
            });
        }
      }
      return res.status(400).json({
        message: "something went's wrong"
      });
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
});

module.exports = router;
